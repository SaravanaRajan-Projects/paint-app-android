package com.paint.ext

import android.app.Activity
import android.content.Intent
import android.provider.MediaStore
import com.paint.models.StorageOptions
import com.paint.utils.AppConstants.SHARE_IMAGE

internal fun Activity.openGallery() {
    val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
    startActivityForResult(gallery, 1)
}

internal fun Activity.shareImage(storageOptions: StorageOptions, filePath: String, fileName: String) {
    val shareIntent = Intent(Intent.ACTION_SEND).apply {
        type = "image/*"
        putExtra(Intent.EXTRA_STREAM, storageOptions.shareUri)
    }
    startActivityForResult(Intent.createChooser(shareIntent, "Share Paint"), SHARE_IMAGE)
}

