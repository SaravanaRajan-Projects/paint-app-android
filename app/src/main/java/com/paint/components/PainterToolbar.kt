package com.paint.components

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.paint.MainActivity
import com.paint.R
import com.paint.controller.PainterController
import com.paint.PensSection

@Composable
internal fun PainterToolbar(
    modifier: Modifier = Modifier,
    painterController: PainterController
) {

    val isColorSelection = remember {
        mutableStateOf(false)
    }

    val isStrokeSelection = painterController.isStrokeSelection.collectAsState()
    val selectedColor = painterController.selectedColor.collectAsState()
    val undonePath = painterController.undonePath.collectAsState()
    val paintPath = painterController.paintPath.collectAsState()
    Column(
        Modifier
            .fillMaxWidth()
            .background(Color.Gray.copy(alpha = 0.5f))
            .animateContentSize()
            .scrollable(
                enabled = isColorSelection.value,
                orientation = Orientation.Vertical,
                // Scrollable state: describes how to consume
                // scrolling delta and update offset
                state = rememberScrollableState { delta ->
                    if (delta < -20) {
                        isColorSelection.value = false
                    }
                    delta
                }
            )
    ) {
        if (isColorSelection.value)
            PensSection(painterController = painterController)
        Row(
            modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            if (isStrokeSelection.value) {
                StrokeSelector(painterController)
            } else {
                val scrollState = rememberScrollState()
                Row(
                    modifier = Modifier
                        .horizontalScroll(scrollState)
                        .padding(horizontal = 16.dp)
                ) {

//                  Stroke Selection
                    IconButton(onClick = {
                        painterController.toggleStrokeSelection()
                    }) {
                        Icon(painterResource(R.drawable.ic_adjust), "stroke")
                    }

//                  Color Selection
                    IconButton(onClick = {
                        if (painterController.isEraserEnabled) {
                            painterController.disableEraser()
                        } else {
                            isColorSelection.value = isColorSelection.value.not()
                        }
                    }) {
                        Image(
                            painterResource(R.drawable.ic_brush),
                            "color",
                            colorFilter = if (painterController.isEraserEnabled)
                                ColorFilter.tint(painterController.prevSelectedColor.collectAsState().value.color)
                            else ColorFilter.tint(selectedColor.value.color)
                        )
                    }

//                  Undo
                    IconButton(
                        onClick = {
                            painterController.undo()
                        },
                        enabled = paintPath.value.isNotEmpty(),
                    ) {
                        Icon(painterResource(R.drawable.ic_undo), "undo")
                    }

//                  Redo
                    IconButton(
                        onClick = {
                            painterController.redo()
                        },
                        enabled = undonePath.value.isNotEmpty(),
                    ) {
                        Icon(painterResource(R.drawable.ic_redo), "redo")
                    }

//                  Clear all
                    IconButton(
                        onClick = {
                            painterController.reset()
                            MainActivity._bitmapImage.value = null
                        },
                        enabled = paintPath.value.isNotEmpty(),
                    ) {
                        Icon(painterResource(R.drawable.ic_clear), "clear all")
                    }

//                  Eraser
                    IconButton(
                        onClick = {
                            painterController.erase()
                        },
                        enabled = paintPath.value.isNotEmpty(),
                    ) {
                        Icon(painterResource(R.drawable.ic_eraser), "eraser")
                    }

//                  Load image
                    IconButton(
                        onClick = {
                            painterController.loadImage()
                        }
                    ) {
                        Icon(painterResource(R.drawable.ic_upload), "laod")
                    }

//                  Share image
                    IconButton(
                        onClick = {
                            painterController.share()
                        }
                    ) {
                        Icon(painterResource(R.drawable.ic_share), "laod")
                    }

//                  Save
                    IconButton(onClick = {
                        painterController.savePaint()
                    }) {
                        Icon(painterResource(R.drawable.ic_save), "save")
                    }
                }

            }
        }
    }
}