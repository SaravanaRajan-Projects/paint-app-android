package com.paint.utils


object AppConstants {
    const val PICK_IMAGE = 101
    const val SHARE_IMAGE = 102
}

object SharedPrefConstant {
    const val PRIVATE_KEY = "PaintPrivateKey"
    const val STROKE_WIDTH = "StrokeWidth"
    const val PAINT_COLOR = "PaintColor"
}

