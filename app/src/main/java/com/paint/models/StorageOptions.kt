package com.paint.models

import android.net.Uri

data class StorageOptions(
    val saveDirectoryName : String = "Canvas Painter",
    val shareDirectoryName : String = "Share Painter",
    val shouldSaveByDefault : Boolean = true,
    var shareUri: Uri = Uri.parse("")
)
